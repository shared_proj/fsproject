package cloudFS;
import java.util.Map;

import cloudFS.avl;
import cloudFS.cloudFS;

public class main {
	
	public static void main(String[] args) {
		cloudFS fs = new cloudFS();
        fs.addFile("root", "i",1);
        fs.addDir("root","dir1");
        fs.addDir("root","qqq");
        fs.addFile("a","root",2);
        fs.addFile("root","i",3);
        fs.addFile("root","i",4);
        fs.addFile("root","h",5);
        fs.addFile("root","e",6);
        fs.addFile("qqq","new1",13);
        fs.addFile("qqq","new2",12);
        fs.addFile("new2", "new3", 14);
        
        fs.addFile("root","d",7);
        fs.addFile("root","j",8);
        fs.addDir("dir1", "dir2");
        fs.addDir("dir2", "dir3");
        fs.addDir("dir3", "dir1");
        fs.addFile("dir3", "deep", 9);
        
     
        fs.showFileSystem();
        fs.printFilesInFileSystem();
        fs.delete("i");
        fs.delete("j");

        fs.delete("dir2");
        fs.delete("new1");

        fs.delete("qqq");

        fs.delete("e");

        fs.delete("d");

        fs.delete("h");
        fs.delete("dir1");
        fs.delete("root");
        fs.delete("as");
        fs.printFilesInFileSystem();
		System.out.println();
        fs.showFileSystem();

	}


}
