package cloudFS;

//class represent entity in Avl DS
public class FsEntity {
	public int height;
	public FsData data;
	public FsEntity left, right;
	public FsEntity parent;
	
	//con't
	public FsEntity(String name,int size,FsEntity parent) {
		this.data = new FsData(name, size);
		this.initVars(parent);
	    
	}
	//con't
	public FsEntity(String name,FsEntity parent) {
		this.data=new FsData(name);
		initVars(parent);
	    
	}
	//inital vars
	public void initVars(FsEntity parent){
		this.height = 1;
	    this.left=null;
	    this.right=null;
	    this.parent=parent;
	}
	
	//getter
	public int getHeight(){
		return height;
	}
	//setter
	public void setLeft(FsEntity left) {
		this.left=left;
	}
	//setter
	public void setRight(FsEntity right) {
		this.right=right;
	}
	//Recursive search from entity by given name
	public FsEntity RSearch(FsEntity entity , String name)
	{
		FsEntity temp=null;
		if (entity != null)
        {
			if(entity.data.getName() == name)
			{
				return entity;
			}
			else if(entity.data.isDirectory()) {
            	temp = entity.RSearch(name);
            }
			if (temp ==null)
			{
				temp = RSearch(entity.left,name);
	            if (temp ==null){
	            	temp = RSearch(entity.right,name);
	            }
	            return temp;
			}
            return temp;
            
        }
		else 
			return temp;
	}
	//print current level of directory
	public void printDirectory()
	{
		this.data.getDirectory().printData(this.data.getDirectory().root);
	}
	//delete entity
	public void delete(String name)
	{
		FsEntity d = search(name);
		if(d!=null)
		{
			this.data.getDirectory().root = this.data.getDirectory().deleteNode(this.data.getDirectory().root, name);
		}
		
	}
	//search enetity in current level of directory
	public FsEntity search(String name)
	{
		
		 return this.data.getDirectory().search(this.data.getDirectory().root, name);
	}
	//Recursive search from root by given name
	public FsEntity RSearch(String name)
	{
		return RSearch(this.data.getDirectory().root,name);
	}
	//get type of Entity
	public boolean isDirectory()
	{
		return true;
	}
	// get root
	public FsEntity getDirectoryRoot()
	{
		return this.data.getDirectory().root;
	}
	
	//add file by given name and entity
	public FsEntity addFile(String name, int size,FsEntity parent)
	{	
		FsEntity entity = new FsEntity(name,size,parent);
		FsEntity res= this.data.getDirectory().insert(this.data.getDirectory().root,entity);
		if(res!=null)
		{
			this.data.getDirectory().root=res;
			return entity;
		}
		return res;
	}
	
	@Override
	public String toString() {
		return data.toString();
	}

	//add directory by given name and entity(parent)
	public FsEntity addDir(String dirName,FsEntity parent)
	{
		FsEntity entity = new FsEntity(dirName,parent);
		FsEntity res= this.data.getDirectory().insert(this.data.getDirectory().root, entity);
		
		if(res!=null)
		{
			this.data.getDirectory().root=res;
			return entity;
		}
		return res;
	}
	

}
