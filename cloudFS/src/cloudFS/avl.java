package cloudFS;

import cloudFS.FsEntity;

class avl {

 FsEntity root;

 // A utility function to get the height of the tree
 int height(FsEntity N) {
     if (N == null)
         return 0;

     return N.height;
 }
 void printData(FsEntity entity)
 {
	 if (entity != null)
     {
		 printData(entity.left);
         System.out.print(entity.data.getName() + " ");
         printData(entity.right);
     }
 }

 int max(int a, int b) {
	 return (a > b) ? a : b;
 }

 FsEntity RR(FsEntity y) {
     FsEntity x = y.left;
     FsEntity T2 = x.right;

     // Perform rotation
     x.right = y;
     y.left = T2;

     // Update heights
     y.height = max(height(y.left), height(y.right)) + 1;
     x.height = max(height(x.left), height(x.right)) + 1;

     // Return new root
     return x;
 }

 //left Rotate
 FsEntity LR(FsEntity x) {
     FsEntity y = x.right;
     FsEntity T2 = y.left;

     // Perform rotation
     y.left = x;
     x.right = T2;

     //  Update heights
     x.height = max(height(x.left), height(x.right)) + 1;
     y.height = max(height(y.left), height(y.right)) + 1;

     // Return new root
     return y;
 }

 // Get Balance factor of FsEntity N
 int getBalance(FsEntity N) {
     if (N == null)
         return 0;

     return height(N.left) - height(N.right);
 }
 
 FsEntity insert(FsEntity entity,FsEntity newEntity) {

     /* 1.  Perform the normal BST insertion */
     if (entity == null)
         return newEntity;
     if(newEntity.data.getName().compareTo(entity.data.getName()) < 0)
    	 entity.left = insert(entity.left, newEntity);
     else if (newEntity.data.getName().compareTo(entity.data.getName()) > 0)
    	 entity.right = insert(entity.right,newEntity);
     else // Duplicate names not allowed
     {
    	 System.err.println(entity.data.getName() + " " +"aleardy in the folder.");
    	 return null;
//    	 return newEntity;
    	 
     }
         
     /* 2. Update height of this ancestor FsEntity */
     entity.height = 1 + max(height(entity.left),
                           height(entity.right));

     /* 3. Get the balance factor of this ancestor
           FsEntity to check whether this FsEntity became
           unbalanced */
     int balance = getBalance(entity);

     // If this FsEntity becomes unbalanced, then there
     // are 4 cases Left Left Case
     if (balance > 1 && newEntity.data.getName().compareTo(entity.left.data.getName())<0)
//     if (balance > 1 && name < FsEntity.left.name)
         return RR(entity);

     // Right Right Case
     if (balance < -1 && newEntity.data.getName().compareTo(entity.right.data.getName())>0)
         return LR(entity);

     // Left Right Case
     if (balance > 1 && newEntity.data.getName().compareTo(entity.left.data.getName())>0) {
    	 entity.left = LR(entity.left);
         return RR(entity);
     }

     // Right Left Case
     if (balance < -1 && newEntity.data.getName().compareTo(entity.right.data.getName())<0) {
    	 entity.right = RR(entity.right);
         return LR(entity);
     }

     /* return the (unchanged) FsEntity pointer */
     return entity;
 }

 FsEntity minValueNode(FsEntity node)
 {
	 FsEntity current = node;

     /* loop down to find the leftmost leaf */
     while (current.left != null)
        current = current.left;

     return current;
 }
 
 FsEntity search(FsEntity entity,String name)
 {
	 while(entity !=null)
	 {
		 if(name.compareTo(entity.data.getName()) < 0)
	    	 entity=entity.left;
	     else if (name.compareTo(entity.data.getName()) > 0)
	    	 entity=entity.right;
	     else
	    	 break;
	 }
	 return entity;
 }
 
 FsEntity deleteNode(FsEntity entity, String name)
 {
     if (entity == null)
         return entity;

     if (name.compareTo(entity.data.getName()) < 0)
    	 entity.left = deleteNode(entity.left, name);

     else if (name.compareTo(entity.data.getName()) > 0)
    	 entity.right = deleteNode(entity.right, name);
     else
     {
         if ((entity.left == null) || (entity.right == null))
         {
        	 FsEntity temp = null;
             if (temp == entity.left)
                 temp = entity.right;
             else
                 temp = entity.left;
             if (temp == null)
             {
                 temp = entity;
                 entity = null;
             }
             else   // One child case
            	 entity = temp; // Copy the contents of
         }
         else
         {
        	 FsEntity temp = minValueNode(entity.right);
             // Copy the data
        	 entity.data = temp.data;
        	 entity.right = deleteNode(entity.right, temp.data.getName());
         }
     }
     if (entity == null)
         return entity;
     entity.height = max(height(entity.left), height(entity.right)) + 1;
     int balance = getBalance(entity);
     if (balance > 1 && getBalance(entity.left) >= 0)
         return RR(entity);

     // LR
     if (balance > 1 && getBalance(entity.left) < 0){
    	 entity.left = LR(entity.left);
         return RR(entity);
     }
     // RR
     if (balance < -1 && getBalance(entity.right) <= 0)
         return LR(entity);
     // RL
     if (balance < -1 && getBalance(entity.right) > 0)
     {
    	 entity.right = RR(entity.right);
         return LR(entity);
     }

     return entity;
 }
 


}


