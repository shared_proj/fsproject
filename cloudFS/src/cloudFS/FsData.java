package cloudFS;

import java.text.SimpleDateFormat;
import java.util.Date;
//class represent data of Entity
public class FsData {
	private int size;
	private String name;
	private Date date;
	private static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("MMM dd HH:mm");
	private avl directory=null;
	private boolean isdir;
	
	//cons't
	public FsData(String name,int size)
	{
		this.size=size;
		this.name=name;
		this.isdir=false;
		this.date=new Date();
	}
	//cons't overload
	public FsData(String name)
	{
		this.name=name;
		this.date=new Date();
		this.size=0;
		this.directory = new avl();
		this.isdir=true;
	}
	//check type of data
	public boolean isDirectory()
	{
		return this.isdir;
	}
	

	@Override
	public String toString() {
		if (this.isDirectory())
			return this.name +" " + GetDate();
		return this.name + " " + this.size + " " + GetDate();
	}
	//getter
	public String GetDate(){
		return DATE_FORMAT.format(date);
	}
	//getter
	public int getSize(){
		return size;
	}
	//getter
	public String getName() {
		return name;
	}
	//getter
	public avl getDirectory()
	{
		return directory;
	}
}
