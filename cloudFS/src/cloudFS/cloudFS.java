package cloudFS;
import java.util.*;
import cloudFS.FsEntity;



public class cloudFS {
	
	private FsEntity root;
	private HashMap<String, FsEntity> hmap = new HashMap<String, FsEntity>();
	
	//con't
	public cloudFS()
	{
		root = new FsEntity("root",null);
		this.hmap.put("root", root);
		printIntro();
		
	}
	//add file by given dirName and parentDirName
	public void addFile (String parentDirName, String fileName, int fileSize)
	{
		
		if (fileName.length()<=32) {
			if(!this.hmap.containsKey(fileName)) {
				FsEntity d=null;
				if(parentDirName =="root")
					d=this.root;
				else{
					if(hmap.containsKey(parentDirName))
						d=hmap.get(parentDirName);
				}
					
					if (d !=null && d.data.isDirectory()){
						
						this.hmap.put(fileName, d.addFile(fileName, fileSize,d));				
						}
			}
		}
		else{
			System.err.println("can't add filename which bigger than 32 characters.");
		}
	}
	//add directory by given dirName and parentDirName
	public void addDir(String parentDirName, String dirName)
	{
		if (dirName.length()<=32) {
			if(!this.hmap.containsKey(dirName)) {
				FsEntity d = null;
				if(parentDirName =="root")
					d=this.root;
				else {
					if(hmap.containsKey(parentDirName))
						d=hmap.get(parentDirName);
				}					
					if (d !=null && d.data.isDirectory()){
						this.hmap.put(dirName, d.addDir(dirName,d));	
					}
			}
		}
		else
		{
			System.err.println("can't add dirname which bigger than 32 characters.");
		}
		
	}
	//delete entity by given name
	public void delete (String name)
	{
		FsEntity d = null;
		if(name =="root")
			System.err.println("can't delete root");
		else {
			if(hmap.containsKey(name))
				d=hmap.get(name);
		}
		if (d !=null){
			ArrayList<String> al = new ArrayList<String>();
			al.add(d.data.getName());
			if (d.data.isDirectory())
				GetAllSonsEntities(d.data.getDirectory().root, al);
			d.parent.delete(name);
			removefilesFromMap(al);
			if(this.hmap.containsKey(name)){
				this.hmap.remove(name);
			}
		}
		
	}
	//remove files From Map
	public void removefilesFromMap(ArrayList<String> arr)
	{
		for (String name : arr) {
			if(this.hmap.containsKey(name))
				this.hmap.remove(name);
		}
		
	}
	
	//print FS like tree
	public void showFileSystem()
	{
		showFileSystemTree(this.root, 0);
	}
	//print FS like tree
	public void showFileSystemTree(FsEntity entity,int level) {
		if (entity != null)
        {
			for(int i=0; i<level;i++)
				System.out.print("\t");
			System.out.print("|-");
			System.out.println(entity);
			if(entity.data.isDirectory()) {
				FsEntity d=entity.getDirectoryRoot();
				if(d != null)
					showFileSystemTree(d,level+1);            	
            }

			if( entity.left !=null )
				showFileSystemTree(entity.left,level);
			if ( entity.right !=null )
				showFileSystemTree(entity.right,level);
        }
		
	}
	
	//get all entities of entity by recursive method
	public void  GetAllSonsEntities(FsEntity entity,ArrayList<String> al)
	{
		if (entity != null)
        {
			GetAllSonsEntities(entity.left,al);
			al.add(entity.data.getName());
			if(entity.data.isDirectory()) {
				FsEntity d=entity.getDirectoryRoot();
				if(d != null)
					GetAllSonsEntities(d,al);            	
            }
			GetAllSonsEntities(entity.right,al);
				
        }
		
	}
	//print Folder Data
	public void printFolderData(String parentDirName) {
		FsEntity d;
		if(parentDirName =="root")
			d=this.root;
		else
		 d= root.RSearch(parentDirName);
		if (d !=null && d.data.isDirectory()){
			d.printDirectory();
		}
		
		
	}
	//print all files that in Map
	public void printFilesInFileSystem()
	{
		for (Map.Entry me : hmap.entrySet()) {
            System.out.print(me.getKey() + " ");
          }		
		System.out.println();
	}
	//print into to screen
	public void printIntro(){
        System.out.println( "\n\n");
        System.out.println("\t\t***************************************************************");
        System.out.println("\t\t*                                                             *");
        System.out.println("\t\t*              ~~~~~Welcome to CloudFs~~~~~                   *");
        System.out.println("\t\t*                                                             *");
        System.out.println("\t\t*       1.Root Directory added.                               *");
        System.out.println("\t\t*       2.first directoy should add to root.                  *");
        System.out.println("\t\t*       3.File system use AvlTree and hashmap data structures.*");
        System.out.println("\t\t*        						                              *");
        System.out.println("\t\t*                ****Matan Ramrazker****                      *");
        System.out.println("\t\t***************************************************************");
        System.out.println( "\n\n");
	}
	
	

}
